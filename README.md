# SyncDataService

#### 介绍
数据同步服务，当前版本只支持Sqlserver数据库的数据同步
市面上的同步软件对sqlserver的支持都不友好；不能很好的解决诸如，如果表含有自增主键、表中有外键约束、表中有触发器等配置，这样表往往会使得，数据同步失败。
这个项目解决了上面这些问题

#### 软件架构
软件架构说明，这是一个通过maven构建的springboot项目


#### 安装教程

1.  git clone https://gitee.com/ggkt/SyncDataService.git
2.  idea 打开工程
3.  File->Project Structure->加载项目模块

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
