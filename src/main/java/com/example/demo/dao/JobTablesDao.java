package com.example.demo.dao;

import com.example.demo.entity.JobTable;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * @author guiguketang
 * @date 2020/11/13 10:04
 */
public interface JobTablesDao {

    //查询需要同步任务的表；
    @Select("select sjt.job_table,sjt.is_identity,sjt.columns,sjd.dbname_read,sjd.dbip_read,sjd.dbname_write,sjd.dbip_write,sjt.id,sjt.job_id,"+
            " sjt.job_table_condition,sjd.sync_type,sdf.column_name,sdf.filter_content,sjt.presql,sjt.postsql"+
            " from sd_job_dtl sjd"+
            " inner join sd_job_table sjt on sjt.job_id=sjd.id"+
            " left join sd_db_filter sdf on sdf.table_name=sjt.job_table"+
            " where sjd.approve_status=1 and sjt.sync_status!=1 limit 5;")
    List<JobTable> selectJobTables() throws Exception;

    //更新任务状态方法；
    @Update("update sd_job_table set sync_status=#{status} where id=#{id}")
    int updateJobTable(int status, int id) throws Exception;

}
