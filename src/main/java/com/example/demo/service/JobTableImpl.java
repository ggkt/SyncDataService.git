package com.example.demo.service;

import com.example.demo.dao.JobTablesDao;
import com.example.demo.entity.JobTable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author guiguketang
 * @date 2020/11/13 11:42
 */
@Service
public class JobTableImpl implements IJobTable {

    @Autowired
    private JobTablesDao jtDao;

    @Override
    public List<JobTable> listJobTables() throws Exception {
        return jtDao.selectJobTables();
    }
}
