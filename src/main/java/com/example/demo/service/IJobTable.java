package com.example.demo.service;

import com.example.demo.entity.JobTable;

import java.util.List;

/**
 * @author guiguketang
 * @date 2020/11/13 11:40
 */
public interface IJobTable {
    List<JobTable> listJobTables() throws Exception;
}
