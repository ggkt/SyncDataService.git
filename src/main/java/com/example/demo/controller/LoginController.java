package com.example.demo.controller;

import com.example.demo.common.SynDataMain;
import com.example.demo.common.SynDataSingle;
import com.example.demo.dao.JobTablesDao;
import com.example.demo.entity.JobTable;
import com.example.demo.service.IJobTable;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import com.example.demo.service.IMember;
import java.util.Date;
import java.util.List;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@PropertySource("classpath:conf/cron.props")
@MapperScan("com.example.demo.dao") //mapper扫描
public class LoginController {

    @Autowired
    IMember iMemberFunc;
    @Autowired
    IJobTable ijTable;
    @Autowired
    private JobTablesDao jtDao;

    @Scheduled(cron = "${jobs.schedule}")
    @RequestMapping(value = "/syncData")
    //@RestController
    public String syncData() {
        Date now = new Date();
        log.info("SyncData Class is executing>>>>>" + now.toString());
        try {
//            List<JobTable> jobTableList = ijTable.listJobTables();
            List<JobTable> jobTableList = jtDao.selectJobTables();
            SynDataMain synData = new SynDataMain();
            SynDataSingle synDataSingle = new SynDataSingle();
            int synType = 0;
            int status = 0;
            for (JobTable jt : jobTableList) {
                System.out.println("the job talbe is===>" + jt.toString());
                synType = jt.getSync_type();
                /**
                 * synType=2为单表同步，synType=3为多表同步；
                 */
                if (synType == 2) {
                    status = synDataSingle.syncDataByJobTable(jt);
                } else if (synType == 3) {
                    status = synData.syncDataByJobTable(jt);
                }
                System.out.println("sync "+jt.getJob_table()+" status is "+status);
                if (status == 1) {
                    int id = jt.getId();
                    int result = jtDao.updateJobTable(status, id);
                    System.out.println("The execution result is:>>>>" + result);
                } else {
                    status = 2;
                    int id = jt.getId();
                    int result = jtDao.updateJobTable(status, id);
                    System.out.println("The execution result is:>>>>" + result);
                }
            }
            log.info("The job table list size is:" + jobTableList.size());
        } catch (Exception e) {
            e.printStackTrace();
        }
        log.info("log ..... invoking sync function");
        return "Go_sync";
    }

    @RequestMapping(value = "/hello")
    public String hello() {
        System.out.println("Hello==========WWWWSSSSSSS");
        return "Log";
    }

}