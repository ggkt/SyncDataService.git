package com.example.demo.test;

import com.example.demo.entity.Member;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author 硅谷课堂
 * @date 2020/11/6 16:29
 * 序列化List对象demo类
 */
public class SerialListTest {
    public static void main(String[] args) {

        List<Member> memList = new ArrayList<Member>();
        for(int i=0;i<10;i++){
            Member member = new Member(i,"name"+i,"password"+i);
            memList.add(member);
        }

        SerialListTest test = new SerialListTest();
        test.serialList(memList);//执行序列化list方法；
        //执行反序列化List方法
        List <Member>list = test.DeSerialList(new File("c:/members.txt"));
        System.out.println("The DeSerialList size is :"+list.size()+" and the value is>>>");
        for(Member memObj:list){
            System.out.println(memObj.toString());
        }

    }


    

    //序列化List对象
    public <T> void serialList(List<T> list){
        //将List转换成数组
        T[] array = (T[]) list.toArray();
        File file = new File("c:/members.txt");
        try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file)))
        {
            out.writeObject(array);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
    //反序列化List对象
    public <T> List<T> DeSerialList(File file){
        T[] object;
        try(ObjectInputStream out = new ObjectInputStream(new FileInputStream(file)))
        {
            object = (T[]) out.readObject();
            return Arrays.asList(object);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        catch (ClassNotFoundException e)
        {
            e.printStackTrace();
        }
        return null;
    }
}
