package com.example.demo.test;

import com.example.demo.entity.Member;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author 硅谷课堂
 * @date 2020/11/6 16:29
 * 序列化对象demo类
 */
public class SerialObject {
    public static void main(String[] args) {

        List<Member> memList = new ArrayList<Member>();
        for(int i=0;i<10;i++){
            Member member = new Member(i,"name"+i,"password"+i);
            memList.add(member);
        }
        Member member = new Member(2,"maYun","password");
        try {
            ObjectOutputStream os = new ObjectOutputStream(
                    new FileOutputStream("C:/member.txt"));
            os.writeObject(member); // 将Member对象写进文件
            os.flush();
            os.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        try {
            ObjectInputStream is = new ObjectInputStream(new FileInputStream("C:/member.txt"));
            Member memberObj = (Member) is.readObject(); // 从流中读取Member的数据
            is.close();
            System.out.println("after Serializable,the member object is: "+memberObj.toString());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }



    }
}
