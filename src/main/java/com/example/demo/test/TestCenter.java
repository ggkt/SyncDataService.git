package com.example.demo.test;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.Properties;

/**
 * @author guiguketang
 * @date 2020/11/19 16:40
 */
public class TestCenter {

    //split and replace test;
        /*      String str1 = "'Alter TABLE qz_statistics_leandata drop constraint FK_QZ_MAJOR_ID','truncate table QZ_MAJOR','alter table QZ_MAJOR DISABLE TRIGGER  all'";
        String str2 = "";
        if(str2!=null && str2.length()>1) {
            String str_[] = str2.split(",");
            System.out.println("the length is:====" + str_.length);
            for (String unit : str_) {
                System.out.println("in for===>" + unit);
                if (unit.length() != 0) {
                    System.out.println(unit.replaceAll("'", ""));
                }
            }
        }*/
    public static void main(String[] args) {
        String base_sql="select * from student  id>10;";
        System.out.println(base_sql.indexOf("where"));

        //TestCenter.readProperty1();
    }

    public static void readProperty1() {
        //InputStream inputStream = new BufferedInputStream(new FileInputStream(new File("src/main/resources/demo/jdbc.properties"))); //方法1
        try {
            Date now = new Date();
            System.out.println(now.toString());
            InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("conf/jdbc.properties"); //方法2
            Properties prop = new Properties();
            prop.load(new InputStreamReader(inputStream)); //加载格式化后的流
            String driverClassName = prop.getProperty("driverClassName");
            System.out.println("Method1: " + driverClassName);
            System.out.println("user name is===>"+prop.getProperty("sqlServer.user"));
            System.out.println("user password==>"+prop.getProperty("sqlServer.password"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void readProperty() {
        try {
            InputStream in = new BufferedInputStream(new FileInputStream("C:\\projects\\jdbc.properties"));
            Properties p = new Properties();
            p.load(in);
            System.out.println(p.getProperty("driverClassName"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //batch test;
    public static void batchTest() {
        int count = 1033;
        int batch = 100;
        System.out.println(count / batch);
        for (int i = 0; i < count / batch + 1; i++) {
            System.out.println(i * 100 + "||" + (i + 1) * 100);
        }
    }

    //split String by ","
    String str = "majorID,businessID,siteID,majorName,shortName,status,sequence,creator,createTime";
    String str_array[] = str.split(",");

    //List<String> new_list = Arrays.asList(str_array);
    //        for(int i=0;i<str_array.length;i++){
    //            if(str_array[i].toLowerCase().indexOf("time")>0) {
    //                System.out.println("the time is "+str_array[i]);
    //            }else {
    //                if (i != str_array.length - 1) {
    //                    System.out.println(str_array[i] + ",");
    //                } else {
    //                    System.out.println(str_array[i]);
    //                }
    //            }
    //        }
}
