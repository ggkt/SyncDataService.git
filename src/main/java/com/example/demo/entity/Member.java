package com.example.demo.entity;

import java.io.Serializable;

/**
 * this is a demo,与同步任务无关；
 */
public class Member implements Serializable {
    private int id;
    private String name;
    private transient String password;

    public Member(){}
    public Member(int id, String name, String password) {
        super();
        this.id = id;
        this.name = name;
        this.password = password;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    @Override
    public String toString() {
        return "Member [id=" + id + ", name=" + name + ", password=" + password
                + "]";
    }

}