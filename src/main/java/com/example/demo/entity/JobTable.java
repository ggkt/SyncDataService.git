package com.example.demo.entity;

import lombok.Data;

/**
 * @author guiguketang
 * @date 2020/11/13 11:00
 * 任务信息实体类
 */
@Data
public class JobTable {
    int id;
    int job_id;
    String columns;
    String job_table;
    int is_identity;
    String dbname_read;
    String dbip_read;
    String dbip_write;
    String dbname_write;
    String job_table_condition;
    int sync_type;
    String column_name;
    String filter_content;
    String presql;
    String postsql;
    @Override
    public String toString(){
        return "JobTable:["+this.id+","+this.job_id+","+this.job_table+","+this.presql+"]";
    }
}
