package com.example.demo.entity;

import lombok.Data;

/**
 * @author guiguketang
 * @date 2020/11/18 16:53
 * 列信息实体类
 */
@Data
public class Column {
    int order;
    String name;
    String type;
    @Override
    public String toString(){
        return "order:"+this.order+"||name:"+this.name+"||type:"+this.type;
    }
}
