/*
Navicat MySQL Data Transfer

Source Server         : 192.168.172.50
Source Server Version : 50505
Source Host           : 192.168.172.50:6006
Source Database       : erp

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2020-11-26 16:14:03
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sd_job_table
-- ----------------------------
DROP TABLE IF EXISTS `sd_job_table`;
CREATE TABLE `sd_job_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `job_id` int(11) DEFAULT NULL COMMENT '任务id 任务表的主键id',
  `job_table` varchar(32) DEFAULT NULL COMMENT '任务表名',
  `job_table_condition` varchar(1024) DEFAULT NULL COMMENT '查询条件',
  `sync_status` tinyint(4) DEFAULT '0' COMMENT '同步状态 0等待执行\n1执行成功\n2执行失败',
  `columns` varchar(1024) DEFAULT NULL COMMENT '表字段',
  `is_identity` tinyint(1) DEFAULT NULL COMMENT 'job_table是否设置了自增',
  `created_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `foreign_key` varchar(80) DEFAULT NULL,
  `has_trigger` tinyint(4) DEFAULT NULL COMMENT '同步表触发器',
  `presql` text COMMENT '同步前sql',
  `postsql` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=utf8 COMMENT='同步任务表记录';
