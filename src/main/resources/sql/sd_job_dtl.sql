/*
Navicat MySQL Data Transfer

Source Server         : 192.168.172.50
Source Server Version : 50505
Source Host           : 192.168.172.50:6006
Source Database       : erp

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2020-11-26 16:13:57
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sd_job_dtl
-- ----------------------------
DROP TABLE IF EXISTS `sd_job_dtl`;
CREATE TABLE `sd_job_dtl` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `dbname_read` varchar(32) DEFAULT NULL COMMENT '读库名',
  `dbip_read` varchar(32) DEFAULT NULL COMMENT '读库ip',
  `dbname_write` varchar(32) DEFAULT NULL COMMENT '写库名',
  `dbip_write` varchar(32) DEFAULT NULL COMMENT '写库ip',
  `sync_type` tinyint(4) DEFAULT NULL COMMENT '同步类型 1全库同步\n2单表同步\n3多表同步',
  `sync_status` tinyint(4) DEFAULT '0' COMMENT '同步状态 0,等待执行\n1,正在执行\n2,执行成功\n3,执行失败',
  `job_remark` varchar(128) DEFAULT NULL COMMENT '任务备注',
  `approve_status` int(10) DEFAULT '0' COMMENT '审批状态,0待审批，1审批通过，2,未通过',
  `created_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `created_userId` int(10) DEFAULT NULL,
  `created_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8 COMMENT='数据同步任务明细';
