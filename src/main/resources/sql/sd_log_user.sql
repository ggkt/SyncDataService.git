/*
Navicat MySQL Data Transfer

Source Server         : 192.168.172.50
Source Server Version : 50505
Source Host           : 192.168.172.50:6006
Source Database       : erp

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2020-11-26 16:14:18
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sd_log_user
-- ----------------------------
DROP TABLE IF EXISTS `sd_log_user`;
CREATE TABLE `sd_log_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `user_name` varchar(20) DEFAULT NULL,
  `content` varchar(1024) DEFAULT NULL COMMENT '日志内容',
  `CREATED_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='用户日志表';
