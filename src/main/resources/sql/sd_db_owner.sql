/*
Navicat MySQL Data Transfer

Source Server         : 192.168.172.50
Source Server Version : 50505
Source Host           : 192.168.172.50:6006
Source Database       : erp

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2020-11-26 16:13:49
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sd_db_owner
-- ----------------------------
DROP TABLE IF EXISTS `sd_db_owner`;
CREATE TABLE `sd_db_owner` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `dbname_read` varchar(128) DEFAULT NULL COMMENT '读库名',
  `user_name` varchar(128) DEFAULT NULL COMMENT '负责人姓名 默认为空，认领后附值',
  `userid_num` varchar(32) DEFAULT NULL COMMENT '负责人工号',
  `remark` varchar(128) DEFAULT NULL COMMENT '备注',
  `db_type` varchar(32) DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `created_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `dbname_read` (`dbname_read`)
) ENGINE=InnoDB AUTO_INCREMENT=680 DEFAULT CHARSET=utf8 COMMENT='数据库认领表';
