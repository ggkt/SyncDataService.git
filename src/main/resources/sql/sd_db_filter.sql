/*
Navicat MySQL Data Transfer

Source Server         : 192.168.172.50
Source Server Version : 50505
Source Host           : 192.168.172.50:6006
Source Database       : erp

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2020-11-26 16:13:42
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sd_db_filter
-- ----------------------------
DROP TABLE IF EXISTS `sd_db_filter`;
CREATE TABLE `sd_db_filter` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `db_name` varchar(32) DEFAULT NULL COMMENT '库名',
  `table_name` varchar(32) DEFAULT NULL COMMENT '表名',
  `column_name` varchar(32) DEFAULT NULL COMMENT '字段名',
  `filter_content` varchar(32) DEFAULT NULL COMMENT '过滤后内容 默认替换为空字符串',
  `created_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `created_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='敏感词过滤设置';
