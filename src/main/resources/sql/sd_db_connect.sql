/*
Navicat MySQL Data Transfer

Source Server         : 192.168.172.50
Source Server Version : 50505
Source Host           : 192.168.172.50:6006
Source Database       : erp

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2020-11-26 16:13:32
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sd_db_connect
-- ----------------------------
DROP TABLE IF EXISTS `sd_db_connect`;
CREATE TABLE `sd_db_connect` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `db_ip` varchar(32) DEFAULT NULL COMMENT '数据库ip',
  `db_username` varchar(32) DEFAULT NULL COMMENT '数据库用户名',
  `db_password` varchar(128) DEFAULT NULL COMMENT '密码',
  `db_type` varchar(32) DEFAULT NULL COMMENT '数据库类型',
  `db_role` char(1) DEFAULT NULL COMMENT '数据库角色 r读\nw写',
  `db_port` varchar(32) DEFAULT NULL COMMENT '端口',
  `db_name` varchar(32) DEFAULT NULL COMMENT '主库名',
  `slave_dbname` varchar(32) DEFAULT NULL COMMENT '从库名',
  `db_remark` varchar(128) DEFAULT NULL COMMENT '备注',
  `created_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `created_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='数据库链接';
